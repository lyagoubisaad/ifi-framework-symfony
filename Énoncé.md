<div align="center">

# Création d'une Application de Forum avec le FrameWork Symfony

</div>

### Dans te Tp nous allons creer un forum, ou les auteurs peuvent choisir un topic et poster un message
## Partie I 
### Creation d'un Crud pour l'entité Topic

#### 1).Installation des dependances necessaires
    Dans votre terminal executez les commandes suivantes :  
    Installez composer et le support Doctrine via le pack orm Symfony, ainsi que le MakerBundle, qui vous aidera à générer du code  
    composer require symfony/orm-pack  
    composer require --dev symfony/maker-bundle  
    composer install
#### 2).Creation d'une base de donnée
    Dans le fichier .env ajouter la ligne ci-dessous  
    DATABASE_URL=mysql://root:motdepassedurootmysql@127.0.0.1:3306/database_name?serverVersion=8.0    
    Dans votre terminal executez la commande suivante : 
    php bin/console doctrine:database:create  
![Alt text](images/Topic_Class.png?raw=true "Title")
#### 3).Creation de l'entity Topic  
    Completer l'entité Topic (src/entity/topic.php)  
        ajouter les attribus suivants :  
            subject String:255  
            description String:255  
        Creer les setters et getters adequats  

#### 4).Creation du formulaire Topic
    Completez le formulaire (src/Form/TopicType.php)
    Dans la methode buildForm ajoutez les attributs subject et description
#### 5).Creation du Controller Topic
    Completez le Controller (src/Controller/TopicController.php)
    Completez les fonctions 
        Index ==> afficher la page topic/index.html.twig
        New ==> afficher le formulaire de creation de Topic (topic/new.html.twig) ou redirigez vers index si le formulaire a ete validé        
        Edit ==> afficher le formulaire de modification de Topic (topic/edit.html.twig) ou redirigez vers la route Topic si le formulaire a ete validé     
        Delete ==> Si le token est valide suprimer l'objet de l'entityManager et redirigrez vers la route Topic_index
#### 6).Lier la base donnée
    Dans votre terminal executez les commandes suivantes : 
    php bin/console make:migration   
    php bin/console doctrine:migrations:migrate  
#### 6).Lancer L'app
    Dans votre terminal executez la commande suivante :    
        symfony server:start
    Dans votre navigateur go to https://localhost:8000/topic et essayer les différentes fonctionnalités développées

## Partie II
### Creation d'un Crud pour l'entité Message
#### 1).Creation de l'entity Message  
    Dans votre terminal executez la commande suivante : 
        php bin/console make:entity  
        Message [author string :255 , message string:255]   
#### 2).Creation du formulaire Message                   
    Dans votre terminal executez la commande suivante : 
        composer require form validator twig-bundle security  
        composer require symfony/form  
        php bin/console make:form  
#### 3).Creation du Controller Message
    Dans votre terminal executez la commande suivante : 
        php bin/console make:controller MessageController  
    Implementez les methodes en vous inspirant des methodes de Topic
        Index
        show 
        edit
        delete
![Alt text](images/Message_class.png?raw=true "Title")

#### 4).Creation des templates Message
    Creer les templates pour l'entité message en vous inspirtant des templates de topic  
    Commencer par creer un dossier message dans le dossier tempaltes
### Mettre en relation les deux entités (Topic 1-N Message)
![Alt text](images/Relation.png?raw=true "Title")

    On souhaite seulement que les messages specifiques au topic soit affiché afin de creer un mini forum
#### 5).Ajouter une relation onetomany (Topic 1--N Message)
    Créer dans Topic un attribut de type message pour y conserver la liste des messages postés dans ce topic
    en vous inspirant de la documentation       
        https://symfony.com/doc/current/reference/forms/types/entity.html#basic-usage  
#### 6).Ajouter une relation manytoone(Message N--1 Topic)
    Dans message, relier les messages à un topic unique  
    Modifiez l'entité message  
    modifier le controller  
    ajouter dans le repository la fonction  findAllByTopicId qui permet de recuperer tous les messages en relation avec un topic specifique avec un $Topicid
#### 7).Lancer votre App
    Comme vu precedemment lancer votre app et n'oublier pas de relier votre Base de donnée (migrate )