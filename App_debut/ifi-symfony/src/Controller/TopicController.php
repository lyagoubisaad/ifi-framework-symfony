<?php

namespace App\Controller;

use App\Entity\Topic;
use App\Form\TopicType;
use App\Repository\TopicRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/topic")
 */
class TopicController extends AbstractController
{
    /**
     * @Route("/", name="topic_index", methods={"GET"})
     */
    public function index(TopicRepository $topicRepository): Response
    {
        // Afficher la page Index topic/index.html.twig

    }

    /**
     * @Route("/new", name="topic_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $topic = new Topic();
        $form = $this->createForm(TopicType::class, $topic);
        $form->handleRequest($request);
        //To Do
        //Enregister le topic et redirigez vers Index
        if ($form->isSubmitted() && $form->isValid()) {

        }
        //To Do 
        // Afficher la page du formulaire topic/new.html.twig

    }


    /**
     * @Route("/{id}/edit", name="topic_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Topic $topic): Response
    {
        $form = $this->createForm(TopicType::class, $topic);
        $form->handleRequest($request);
        //To Do
        //Enregistrez les modfications et Redirigez vers la route Topic
        if ($form->isSubmitted() && $form->isValid()) {

        }
        //Affichez le formulaire de modification topic/edit.html.twig
    }

    /**
     * @Route("/{id}", name="topic_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Topic $topic): Response
    {   
        //To Do
        //Suprimer l'objet de l'entityManager
        if ($this->isCsrfTokenValid('delete'.$topic->getId(), $request->request->get('_token'))) {

        }
        //Redirigez vers la route Topic_Index
    }
}
