<?php

namespace App\Repository;

use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }

    public function findByName(string $nom) {

        return $this->createQueryBuilder('message')
            ->where('message.author LIKE :nom')
            ->setParameter('nom', '%'.$nom.'%')
            ->orderBy('message.id', 'ASC')
            ->getQuery()
            ->execute()
            ;
    }

    public function findAllByTopicId(int $topicid) {
        return $this->createQueryBuilder('message')
            ->where('message.topic = :topicid')
            ->setParameter('topicid', $topicid)
            ->orderBy('message.id', 'ASC')
            ->getQuery()
            ->execute()
            ;
    }
}
