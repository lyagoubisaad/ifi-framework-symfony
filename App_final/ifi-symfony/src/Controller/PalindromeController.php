<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
class PalindromeController extends AbstractController
{
    /**
     *@Route("/palindrome/{mot}")
     */
    public function palindrome(String $mot): Response
    {   
        if (strrev($mot) == $mot){   
            return new Response(
                '<html><body>The word : '.$mot.' is a palindrome</body></html>'
            );   
        } 
        else{ 
            return new Response(
                '<html><body>The word : '.$mot.' is not a palindrome</body></html>'
            ); 
        } 
    }  
}