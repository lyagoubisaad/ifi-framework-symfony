<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\MessageType;
use App\Repository\MessageRepository;
use App\Repository\TopicRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/topic/{topicid}/message")
 */
class MessageController extends AbstractController
{
    /**
     * @Route("/", name="message_index", methods={"GET"})
     */
    public function index(MessageRepository $messageRepository,int $topicid): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        \Doctrine\Common\Util\Debug::dump($topicid);
        return $this->render('message/index.html.twig', [
            'messages' => $messageRepository->findAllByTopicId($topicid),
            'topicid' => $topicid,
        ]);
    }

        /**
     * @Route("/name/{givenname}", name="author_search", methods={"GET"})
     */
    public function search(MessageRepository $messageRepository , string $givenname, int $topicid): Response
    {
        $tab[] = $messageRepository->findbyName($givenname);
        return $this->render('message/index.html.twig', [
            'messages' => $messageRepository->findbyName($givenname),
        ]);
        
    }

    /**
     * @Route("/new", name="message_new", methods={"GET","POST"})
     */
    public function new(TopicRepository $topicRepository ,Request $request, int $topicid): Response
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $message->setTopic($topicRepository->find($topicid));
            $entityManager->persist($message);
            $entityManager->flush();

            return $this->redirectToRoute('message_index', ['topicid' => $topicid]);
        }

        return $this->render('message/new.html.twig', [
            'message' => $message,
            'form' => $form->createView(),
            'topicid' => $topicid,
        ]);
    }

    /**
     * @Route("/{id}", name="message_show", methods={"GET"})
     */
    public function show(Message $message, int $topicid): Response
    {
        return $this->render('message/show.html.twig', [
            'message' => $message,
            'topicid' => $topicid,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="message_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Message $message, int $topicid): Response
    {
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('message_index' , ['topicid' => $topicid]);
        }

        return $this->render('message/edit.html.twig', [
            'message' => $message,
            'form' => $form->createView(),
            'topicid' => $topicid,
        ]);
    }

    /**
     * @Route("/{id}", name="message_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Message $message, int $topicid): Response
    {
        if ($this->isCsrfTokenValid('delete'.$message->getId(), $request->request->get('_token'))) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($message);
            $entityManager->flush();

        }

        return $this->redirectToRoute('message_index', ['topicid' => $topicid]);
    }
}
